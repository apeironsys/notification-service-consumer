# README #

Example consumer implementation for Apeiron Systems Notification Service based on [Flask](http://flask.pocoo.org/). It can be configured to forward all requests to an email.

### Installation ###
    $ python setup.py install

### Running ###
    $ apeironnsconsumer

### Forward requests to an email using local SMTP server ###
    $ SEND_TO='your@email.com' apeironnsconsumer

### Forward requests to an email using Gmail SMTP server ###
    $ SEND_TO='your@email.com' \
    SMTP_HOST='smtp.gmail.com' \
    SMTP_PORT='587' \
    SMTP_TLS=1 \
    SMTP_USER='*****@gmail.com' \
    SMTP_PASS='*****' \
    apeironnsconsumer

### Support ###

* Feel free to [contact us](https://apeironsys.com/contact-us/).