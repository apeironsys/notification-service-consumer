from setuptools import setup, find_packages

setup(
    name='ApeironNSConsumer',
    version='0.0.1',

    author='Slava Yanson',
    author_email='sy@apeironsys.com',
    url='https://apeironsys.com',

    packages=find_packages(),
    include_package_data=True,

    install_requires=[
        'flask'
    ],

    entry_points={
        'console_scripts': [
            'apeironnsconsumer=ApeironNSConsumer:main'
        ]
    },

    classifiers=[
        'Operating System :: MacOS :: MacOS X',
        'Operating System :: Unix',
        'Operating System :: POSIX',
        'Programming Language :: Python'
    ]
)
