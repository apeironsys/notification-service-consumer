from flask import Flask, request, json
import re
import os
import smtplib
from email.mime.text import MIMEText


app = Flask(__name__)
# app.debug = True


def _forward_to_email(headers, json_data):
    """
    Forward request data to an email
    """
    if 'SEND_TO' not in os.environ:
        # SEND_TO was not provided, exit
        return

    params = {
        'host': os.environ.get('SMTP_HOST', 'localhost')
    }

    if 'SMTP_PORT' in os.environ:
        params['port'] = int(os.environ.get('SMTP_PORT'))

    smtp_host = smtplib.SMTP(**params)

    if os.environ.get('SMTP_TLS', '0') == '1':
        smtp_host.ehlo()
        smtp_host.starttls()

    if 'SMTP_USER' in os.environ and 'SMTP_PASS' in os.environ:
        smtp_host.login(os.environ.get('SMTP_USER'), os.environ.get('SMTP_PASS'))

    sender = os.environ.get('SEND_FROM', 'Apeiron Systems<noreply@apeironsys.com>')
    receiver = os.environ.get('SEND_TO')

    msg = MIMEText('%s\n\n\n%s' % (headers, json_data))
    msg['Subject'] = os.environ.get('SEND_SUBJECT', 'Apeiron NS Consumer request')
    msg['From'] = sender
    msg['To'] = receiver

    smtp_host.sendmail(sender, [receiver], msg.as_string())
    smtp_host.quit()


def handle_request(json_data):
    """
    Handle the request
    """
    def _response(response_dict):
        """ Encode the response """
        return json.dumps(response_dict)

    def _error(error_string):
        """ Format an error response """
        return _response({'error': error_string})

    try:
        # decode the request
        data = json.loads(json_data)
    except ValueError:
        return _error('Malformed request')

    if 'method' not in data:
        # make sure method parameter exists in request
        return _error('Invalid request')
    else:
        # sanitize method parameter
        method = re.sub(r'[^a-zA-Z0-9\-_]', '', data['method'])

    ### method handling starts here ###

    if method == 'ping':
        # handle "ping" request
        return _response({'response': 'pong'})
    else:
        # we don't know how to handle this method
        return _error('"%s" is unsupported' % method)


@app.route('/', methods=['POST'])
def consumer():
    """ Consume Notification Service request """
    _forward_to_email(request.headers, request.data)  # forward request to email
    return handle_request(request.data)


def main():
    app.run()


if __name__ == "__main__":
    main()
