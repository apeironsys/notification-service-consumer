import ApeironNSConsumer
import unittest
from flask import json


class ApeironNSConsumerTestCase(unittest.TestCase):
    def setUp(self):
        ApeironNSConsumer.app.config['TESTING'] = True
        self.app = ApeironNSConsumer.app.test_client()

    def tearDown(self):
        pass

    def test_get(self):
        rq = self.app.get('/')
        assert rq.status_code == 405

    def test_malformed(self):
        rq = self.app.post('/', data='')
        assert json.loads(rq.data)['error'] == 'Malformed request'

    def test_no_method(self):
        rq = self.app.post('/', data=json.dumps({}))
        assert json.loads(rq.data)['error'] == 'Invalid request'

    def test_ping(self):
        rq = self.app.post('/', data=json.dumps({'method': 'ping'}))
        assert json.loads(rq.data)['response'] == 'pong'


if __name__ == '__main__':
    unittest.main()
